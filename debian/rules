#!/usr/bin/make -f

%:
	dh $@

SRC_FILES := $(shell find . \( -path "./debian" -o -path "./.*" \) -prune -o -type f -print)
JS_MINFILES = $(patsubst %.js,%.min.js,$(filter-out %.min.js,$(filter %.js,$(SRC_FILES))))
CSS_MINFILES = $(patsubst %.css,%.min.css,$(filter-out %.min.css,$(filter %.css,$(SRC_FILES))))
, = ,

# Minify JS: generate .min.js from .js source (cf. bin/jsshrink.sh)
$(JS_MINFILES): %.min.js: %.js
	uglifyjs --compress --mangle --source-map "base='$(@D)'$(,)url='$(@F).map'" \
		-o $@ -- $<

# Minify CSS: generate .min.css from .css source (cf. bin/cssshrink.sh)
CSS_STAMPFILE = updatecss-stamp
$(CSS_MINFILES): %.min.css: %.css $(CSS_STAMPFILE)
	cd $(@D) && cleancss --source-map -o $(@F) -- $(<F)

updatecss: $(CSS_STAMPFILE)
$(CSS_STAMPFILE): $(filter-out %.min.css,$(filter %.css,$(SRC_FILES)))
	bin/cssimages.sh
	touch -- $@
.PHONY: updatecss

GENERATED_FILES = $(JS_MINFILES) $(CSS_MINFILES)

GENERATED_MAPFILES = $(addsuffix .map,$(JS_MINFILES) $(CSS_MINFILES))
GENERATED_FILES += $(GENERATED_MAPFILES)
$(GENERATED_MAPFILES): %.map: | % ;

# Pre-compress minified files
COMPRESSED_FILES := $(addsuffix .gz,$(JS_MINFILES) $(CSS_MINFILES))
GENERATED_FILES += $(COMPRESSED_FILES)
$(COMPRESSED_FILES): %.gz: %
	pigz -11 -mnk -- $<

execute_after_dh_auto_clean:
	@rm -f -- $(GENERATED_FILES) $(CSS_STAMPFILE)
	# undo bin/cssimages.sh
	find $(CURDIR) -name "*.css" -execdir sed -ri "s#\\b(url\\(([\"']?)[a-zA-Z0-9_./-]*\\.(gif|ico|png|jpg|jpeg))\\?v=[^)\"']+(\\2\\))#\\1\\4#" {} +

override_dh_auto_build: $(GENERATED_FILES) ;
.SECONDARY:
